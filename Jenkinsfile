def get_email() {
   dir(path: "${env.REPO}" ) {
      return sh (script:"git log -1 --pretty=format:'%ae'",returnStdout:true).trim()
   }
}  
pipeline {
  agent {
    docker {
      image 'sord/devops:gcovr'
    }

  }
  stages {
      stage('Building base-askap (Debug)') {
      steps {
        dir(path: '.') {
          sh '''git fetch --tags
mkdir build
cd build
cmake -DCMAKE_INSTALL_PREFIX=${PREFIX} -DCMAKE_BUILD_TYPE=Debug -DCMAKE_CXX_FLAGS="-coverage" ../
make 
'''
        }
      }
    }

    stage('Building base-askap (Release)') {
      steps {
        dir(path: '.') {
          sh '''git fetch --tags
mkdir build-release
cd build-release
cmake -DCMAKE_INSTALL_PREFIX=${PREFIX} -DCMAKE_BUILD_TYPE=Release ../
make 
'''
        }
      }
    }
    stage('Test') {
     steps {
        dir(path: '.') {
          sh '''cd build
tar -xjf ${WORKSPACE}/all_yandasoft/base-accessors/measdata.tar.bz2
export CASARCFILES=${PWD}/tmp.casarc
echo 'measures.directory: '${PWD}'/data' > ${CASARCFILES}
ctest -T test --no-compress-output
../askap-cmake/ctest2junit > ctest.xml
          cp ctest.xml $WORKSPACE
'''     }
        dir(path: '.') {
          sh '''gcovr -r . \
--xml-pretty \
--exclude-unreachable-branches \
-o coverage.xml 
'''
        }
        step([
            $class: 'CoberturaPublisher',
            coberturaReportFile: 'coverage.xml',
            failNoReports: false,
            failUnhealthy: false,
            failUnstable: false,
            maxNumberOfBuilds: 100
        ])
      }
    }
  }

post {

        always {
             junit 'ctest.xml'
        }        
        success {        
             mail to: "${env.EMAIL_TO}",
             from: "jenkins@csiro.au",
             subject: "Succeeded Pipeline: ${currentBuild.fullDisplayName}",
             body: "Build ${env.BUILD_URL} succeeded"
 
        }

        failure {        
             mail to: "${env.EMAIL_TO}",
             from: "jenkins@csiro.au",
             subject: "Failed Pipeline: ${currentBuild.fullDisplayName}",
             body: "Something is wrong with ${env.BUILD_URL}"
 
        }
        unstable {
             mail to: "${env.EMAIL_TO}",
             from: "jenkins@csiro.au",
             subject: "Unstable Pipeline: ${currentBuild.fullDisplayName}",
             body: "${env.BUILD_URL} unstable"
        } 
        changed {
             mail to: "${env.EMAIL_TO}",
             from: "jenkins@csiro.au",
             subject: "Changed Pipeline: ${currentBuild.fullDisplayName}",
             body: "${env.BUILD_URL} changed"
        }
 }
 
  environment {
    
    WORKSPACE = pwd()
    PREFIX = "${WORKSPACE}/install"
    REPO = "${WORKSPACE}/base-askap/"
    EMAIL_TO = get_email()

  }
}

