/// @file AskapImageType.h
/// @brief ASKAP Image Value Type definition
///
/// @copyright (c) 2021 CSIRO
/// Australia Telescope National Facility (ATNF)
/// Commonwealth Scientific and Industrial Research Organisation (CSIRO)
/// PO Box 76, Epping NSW 1710, Australia
/// atnf-enquiries@csiro.au
///
/// This file is part of the ASKAP software distribution.
///
/// The ASKAP software distribution is free software: you can redistribute it
/// and/or modify it under the terms of the GNU General Public License as
/// published by the Free Software Foundation; either version 2 of the License,
/// or (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with this program; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
///
/// @author Mark Wieringa
///

#ifndef ASKAP_IMAGE_TYPE_H
#define ASKAP_IMAGE_TYPE_H

// CASACORE
#include <casacore/casa/aips.h>

// The default image value types for internal storage as parameters and for
// gridding/fft operations are double and DComplex even though externally
// images are always stored in single precision.
// This file lets you use the compiler switch ASKAP_FLOAT_IMAGE_PARAMS to
// switch the internal storage to single precision to save memory and avoid
// multiple float<->double conversions. The possible negative consequences
// of this are yet to be explored.

namespace askap {
    #ifdef ASKAP_FLOAT_IMAGE_PARAMS
    typedef float imtype;
    typedef casacore::Complex imtypeComplex;
    #else
    typedef double imtype;
    typedef casacore::DComplex imtypeComplex;
    #endif
} // end namespace askap
#endif
