/// @file
/// 
/// @brief Legacy base classes for STL predicated removed in C++17       
/// @details We want to retain ability to build under various C++ standards.
/// Originally predicate classes (e.g. see IndexedCompare) were written inherited from STL types
/// which no longer available. This include either defines those types in our namespace or
/// imports from STL if build under old standard.
///
/// @copyright (c) 2007 CSIRO
/// Australia Telescope National Facility (ATNF)
/// Commonwealth Scientific and Industrial Research Organisation (CSIRO)
/// PO Box 76, Epping NSW 1710, Australia
/// atnf-enquiries@csiro.au
///
/// This file is part of the ASKAP software distribution.
///
/// The ASKAP software distribution is free software: you can redistribute it
/// and/or modify it under the terms of the GNU General Public License as
/// published by the Free Software Foundation; either version 2 of the License,
/// or (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with this program; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
///
/// @author Max Voronkov <maxim.voronkov@csiro.au>

#ifndef ASKAP_ASKAP_LEGACY_BASE_PREDICATES_H
#define ASKAP_ASKAP_LEGACY_BASE_PREDICATES_H

#include <functional>

namespace askap {

namespace utility {

#if __cplusplus > 201402L

// C++17 removed binary_function so define it here to get
// the code to compile and build
template< class Arg1, class Arg2, class Result > 
struct binary_function
{
    using first_argument_type=Arg1;
    using second_argument_type=Arg2;
    using result_type=Result;
};

template< typename ArgumentType, typename ResultType >
struct unary_function
{
    using argument_type = ArgumentType;
    using result_type = ResultType;
};
#else

// import from std to the current namespace

template< typename Arg1, typename Arg2, typename Result > 
using binary_function = std::binary_function<Arg1, Arg2, Result>; 

template< typename Argument, typename Result >
using unary_function = std::unary_function<Argument, Result>;

#endif

} // namespace utility

} // namespace askap

#endif // #define ASKAP_ASKAP_LEGACY_BASE_PREDICATES_H
